class Employee{
    constructor(name, age, salary){
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name (){
        return this._name;
    }
    get age () {
        return this._age;
    }
    get salary () {
        return this._salary;
    }
    set name(value){
        while(value === ''){
            console.log('Input a correct name')
        };
        this._name = value;
    }
    set age (value) {
        while (value<18){
            console.log('you are very young')
        };
        this._age = value;
    }
    set salary (value) {
        while (value<0){
            console.log('Input the correct number of salary')
        };
        this._salary = value;
    }
};

class Programmer extends Employee {
    constructor(name, age, salary, lang){
        super(name, age);
        this.newSalary = salary; 
        this.lang = lang;
    }
    
    set newSalary (value) {
        this._newSalary = value*3;
    }
    get newSalary (){
        return this._newSalary;
    }
}
let programmer_1 = new Programmer('Inna', 35, 10000, 3);
let programmer_2 = new Programmer('Rita',26, 8000, 3);
let programmer_3 = new Programmer('Maxim',41, 12000, 2);
console.log(programmer_1, programmer_2, programmer_3);